
#include <AzCore/Serialization/SerializeContext.h>

#include "SobeSystemComponent.h"

#include <Sobe/SobeTypeIds.h>

namespace Sobe
{
    AZ_COMPONENT_IMPL(SobeSystemComponent, "SobeSystemComponent",
        SobeSystemComponentTypeId);

    void SobeSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (auto serializeContext = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serializeContext->Class<SobeSystemComponent, AZ::Component>()
                ->Version(0)
                ;
        }
    }

    void SobeSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC_CE("SobeService"));
    }

    void SobeSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC_CE("SobeService"));
    }

    void SobeSystemComponent::GetRequiredServices([[maybe_unused]] AZ::ComponentDescriptor::DependencyArrayType& required)
    {
    }

    void SobeSystemComponent::GetDependentServices([[maybe_unused]] AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
    }

    SobeSystemComponent::SobeSystemComponent()
    {
        if (SobeInterface::Get() == nullptr)
        {
            SobeInterface::Register(this);
        }
    }

    SobeSystemComponent::~SobeSystemComponent()
    {
        if (SobeInterface::Get() == this)
        {
            SobeInterface::Unregister(this);
        }
    }

    void SobeSystemComponent::Init()
    {
    }

    void SobeSystemComponent::Activate()
    {
        SobeRequestBus::Handler::BusConnect();
    }

    void SobeSystemComponent::Deactivate()
    {
        SobeRequestBus::Handler::BusDisconnect();
    }
}
