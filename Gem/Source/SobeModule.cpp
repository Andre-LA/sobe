
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include "SobeSystemComponent.h"

#include <Sobe/SobeTypeIds.h>

namespace Sobe
{
    class SobeModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(SobeModule, SobeModuleTypeId, AZ::Module);
        AZ_CLASS_ALLOCATOR(SobeModule, AZ::SystemAllocator);

        SobeModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                SobeSystemComponent::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<SobeSystemComponent>(),
            };
        }
    };
}// namespace Sobe

AZ_DECLARE_MODULE_CLASS(Gem_Sobe, Sobe::SobeModule)
