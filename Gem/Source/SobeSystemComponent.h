
#pragma once

#include <AzCore/Component/Component.h>

#include <Sobe/SobeBus.h>

namespace Sobe
{
    class SobeSystemComponent
        : public AZ::Component
        , protected SobeRequestBus::Handler
    {
    public:
        AZ_COMPONENT_DECL(SobeSystemComponent);

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

        SobeSystemComponent();
        ~SobeSystemComponent();

    protected:
        ////////////////////////////////////////////////////////////////////////
        // SobeRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
