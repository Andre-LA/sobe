
#pragma once

#include <Sobe/SobeTypeIds.h>

#include <AzCore/EBus/EBus.h>
#include <AzCore/Interface/Interface.h>

namespace Sobe
{
    class SobeRequests
    {
    public:
        AZ_RTTI(SobeRequests, SobeRequestsTypeId);
        virtual ~SobeRequests() = default;
        // Put your public methods here
    };

    class SobeBusTraits
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static constexpr AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static constexpr AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////
    };

    using SobeRequestBus = AZ::EBus<SobeRequests, SobeBusTraits>;
    using SobeInterface = AZ::Interface<SobeRequests>;

} // namespace Sobe
