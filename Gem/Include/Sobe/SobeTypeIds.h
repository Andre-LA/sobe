
#pragma once

namespace Sobe
{
    // System Component TypeIds
    inline constexpr const char* SobeSystemComponentTypeId = "{D3C0776F-94E6-4C70-9EF3-8F681BF21008}";

    // Module derived classes TypeIds
    inline constexpr const char* SobeModuleTypeId = "{33BAA837-D7F0-4360-8DD2-C29BF97920EB}";

    // Interface TypeIds
    inline constexpr const char* SobeRequestsTypeId = "{02CC8921-DB2D-4473-AF44-4C1F6A591D44}";
} // namespace Sobe
